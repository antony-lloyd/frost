<?php
namespace Frost;

class FrostException extends \Exception
{
	public function __construct($message = NULL, $code = 0)
	{
		parent::__construct($message, $code);

		echo "Frost ERROR: {$message}. At: {$this->getFile()} line {$this->getLine()}".debug_print_backtrace();
		die();
	}
}