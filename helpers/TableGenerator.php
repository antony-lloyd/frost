<?php
namespace Frost\Helpers;

/**
 * Creates an HTML table from dbObjects
 */
class TableGenerator
{
	private $cols;
	private $colTitles;
	private $data;
	private $customFunctions;

	public function __construct()
	{
		$this->customFunctions 						= [ ];
	}

	/**
	 * Sets the columns of the table
	 * @param array $cols The table columns as a key value pair. Title => dbObject get
	 */
	public function setColumns(array $cols)
	{
		$this->cols 								= $cols;
		$this->colTitles 							= array_keys($cols);
	}

	/**
	 * Sets the data of the table
	 * @array $data An array of dbObjects
	 */
	public function setData(array $data)
	{
		$this->data 								= $data;
	}

	/**
	 * Add a custom function for formatting dbObject data
	 * @param string $name The name of the function
	 * @param object $callback The callback function
	 */
	public function addFunction($name, $callback)
	{
		$this->customFunctions[$name] 				= $callback;
	}

	/**
	 * Generates the table HTML
	 * @param string $id The ID to give the table
	 * @param string $classStr The class string to give the table
	 */
	public function generate($id = '', $classStr = '')
	{
		$table 										= "<table id='{$id}' class='{$classStr}'><thead><tr>";


		foreach($this->colTitles as $col)
			$table 									.= "<th>{$col}</th>";

		$table 										.= '</tr></thead><tbody>';

		foreach($this->data as $obj)
		{
			$table 									.= '</tr><tr>';

			foreach($this->colTitles as $col)
			{
				$colName 							= $this->cols[$col];

				// Check if there is a custom function
				if(isset($this->customFunctions[$colName]))
					$value 						= $this->customFunctions[$colName]($obj);
				else
					$value 						= $obj->$colName();

				$table 							.= "<td>{$value}</td>";
			}
		}

		return $table.'</tr></tbody></table>';
	}
}