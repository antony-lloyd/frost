<?php
namespace Frost\Helpers;

/**
 * Generates a AJAX response
 */
class AjaxResponse
{
	private $response;

	public function __construct()
	{
		$this->repsonse 							= [ ];
		$this->setSuccess(true);
	}

	/**
	 * Adds a notification to the response
	 * @param string $msg The message to show
	 * @param int $type a Frost Notification type
	 */
	public function addNotification($msg, $type)
	{
		$this->response['notification'][] 			= [
			'value' 		=> Notification::create($msg, $type),
			'type'			=> Notification::getTypeText($type)
		];
	}

	/**
	 * Sets the redirect value
	 * @param string $location The URL to redirect to
	 */
	public function setRedirect($location)
	{
		$this->response['redirect'] 				= $location;
	}

	/**
	 * Sets the succcess status
	 * @param bool $success The success state
	 */
	public function setSuccess($success)
	{
		$this->response['success'] 					= $success;
	}

	/**
	 * Adds data to the repsonse
	 * @param string $name The data key
	 * @param mixed $data The data to set
	 */
	public function addData($name, $data)
	{
		$this->response[$name] 						= $data;
	}

	/**
	 * Send the response
	 */
	public function send()
	{
		echo json_encode($this->response);
	}

	/**
	 * Sends the response and immediatly stops execution
	 */
	public function sendAndDie()
	{
		$this->send();
		die();
	}
}