<?php
namespace Frost;

spl_autoload_register(function($class) {
	$parts 						= (explode('\\', $class));
	$file 						= end($parts);

	// Check what class to load
	switch($parts[0])
	{
		case 'Frost':
			loadFrost($parts[1], $file);
		break;
		case 'Theme':
			loadTheme($file);
		break;
		default:
			loadConfig($parts[0], $parts[1], $file);
			break;
	}
});

/**
 * Loads a Frost framework class
 * @param string $namespace The namespace of the Frost class
 * @param string $file The class file name
 */
function loadFrost($namespace, $file)
{
	// An array of available Frost namespaces
	$namespaces 				= [
		'Auth' 		=> 'auth/',
		'Object' 	=> 'objects/',
		'Db'		=> 'db/',
		'Hmvc' 		=> 'hmvc/',
		'Helpers'	=> 'helpers/'
	];

	// Check if the class is in a Frost namespace
	if(array_key_exists($namespace, $namespaces))
	{
		// Check if the class exists
		if(file_exists(FROST_ROOT."{$namespaces[$namespace]}{$file}.php"))
		{
			require_once(FROST_ROOT."{$namespaces[$namespace]}{$file}.php");
			return;
		}
	}
	// Otherwise check in the root namespace
	else
	{
		if(file_exists(FROST_ROOT."{$file}.php"))
		{
			require_once(FROST_ROOT."{$file}.php");
			return;
		}
	}
}

/**
 * Loads an Theme class
 * @param string $file The class file name
 */
function loadTheme($file)
{
	if(file_exists(THEME_ROOT.Config::getSetting('SITE_THEME')."/templates/{$file}.php"))
		require_once(THEME_ROOT.Config::getSetting('SITE_THEME')."/templates/{$file}.php");
}

/**
 * Loads a app from the autoload config file
 * @param $appName The app name (first part of namespace)
 * @param $featureName The app feature (second part of namespace)
 * @param $file The file to load
 */
function loadConfig($appName, $featureName, $file)
{
	if(!file_exists(CONFIG_ROOT.'autoload.json'))
		return;

	$data = json_decode(file_get_contents(CONFIG_ROOT.'autoload.json'), true);

	if(!isset($data[$appName]))
		return;

	// Remember the app name
	$GLOBALS['FROST']['APP_NAME']			= $appName;

	if(file_exists($_SERVER['DOCUMENT_ROOT']."/{$data[$appName]}/{$featureName}/{$file}.php"))
		require_once($_SERVER['DOCUMENT_ROOT']."/{$data[$appName]}/{$featureName}/{$file}.php");
}