<?php
namespace Frost;

session_start();

// Declare
define('FROST_VERSION',			0.1);
define('FROST_ROOT', 			$_SERVER['DOCUMENT_ROOT']."/frost/");
define('CONFIG_ROOT', 			$_SERVER['DOCUMENT_ROOT']."/cfg/");
define('THEME_ROOT', 			$_SERVER['DOCUMENT_ROOT']."/public/themes/");
define('THEME_RELATIVE_ROOT', 	"/public/themes/");
define('UPLOAD_ROOT', 			$_SERVER['DOCUMENT_ROOT']."/uploads/");

require_once('Autoload.php');

Config::readConfig();

if(Config::getSetting('INSTALL_REQUIRED') == 'true')
{
	require_once('installer/Install.php');
	die();
}

require_once('../public/index.php');