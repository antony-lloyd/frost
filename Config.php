<?php
namespace Frost;

class Config
{
	private static $instance 			= NULL;
	private static $config 				= NULL;

	private function __construct()
	{

	}

	private static function getInstance()
	{
		if(self::$instance == NULL)
			self::$instance 			= new Config();
		return self::$instance;
	}

	public static function readConfig()
	{
		if(!file_exists(CONFIG_ROOT.'frost.json'))
			throw new FrostException('Cannot find config: \'frost.json\'');

		$configTxt						= file_get_contents(CONFIG_ROOT.'frost.json');

		self::$config 					= json_decode($configTxt, true);
	}

	public static function getSetting($name)
	{
		if(!isset(self::$config[$name]))
			return false;

		return self::$config[$name];
	}
}