<?php
namespace Frost\Object;

/**
 * File categories
 */
class Category extends \Frost\Db\dbObject
{
	private $db;

	public function __construct($objId = NULL)
	{
		parent::__construct($objId);
	}

	protected function getTableName()
	{
		return 'frost_file_category';
	}

	/**
	 * Gets an array of all categories
	 * @return array An array of file objects
	 */
	public function getAllFiles()
	{
		$files 										= $this->db->prepareExecute('SELECT id FROM frost_file WHERE category = :cid', [ 'cid' => $this->getId() ])->fetchAll(PDO::FETCH_COLUMN);

		$filesArr 									= [ ];

		foreach($files as $file)
			$filesArr[] 							= new File($file);

		return $filesArr;
	}
}