<?php
namespace Frost\Object;

define('UPLOAD_MAX_SIZE', \Frost\Config::getSetting('UPLOAD_MAX_SIZE'));

/**
 * Frost files
 */
class File extends \Frost\Db\dbObject
{
	const ERROR_UPLOAD_EMPTY_PARAMS 				= 0;
	const ERROR_UPLOAD_INVALID_EXTENSION 			= -1;
	const ERROR_UPLOAD_TOO_BIG 						= -2;
	const ERROR_UPLOAD_MOVE 						= -3;

	protected function getTableName()
	{
		return 'frost_file';
	}

	/**
	 * Gets the Category dbObject of the file
	 * @return \Frost\File\Category The category object
	 */
	public function getCategoryObject()
	{
		return new Category($this->getCategory());
	}

	/**
	 * Uploads a file
	 * @param array $file The $_FILES file to upload
	 * @param array $allowedExtensions An array of allowed extensions (excluding period ('.'))
	 * @param string $uploadedName The path & name to upload as. Can have multiple sub-directories, name should be the last avaiable part (without extension)
	 * @param int $maxSize The max file size to upload. Initially set to config UPLOAD_MAX_SIZE
	 * @return mixed True on success, error code on fail
	 */
	public function upload(array $file, array $allowedExtensions, $uploadedName, $maxSize = UPLOAD_MAX_SIZE)
	{
		// Check for required paramaters
		if(!isset($file) || !isset($allowedExtensions) || !isset($uploadedName))
			return self::ERROR_UPLOAD_EMPTY_PARAMS;

		// Check if the extension is allowed
		$extension 									= strtolower(end(explode('.', $file['name'])));

		if(!in_array($extension, $allowedExtensions))
			return self::ERROR_UPLOAD_INVALID_EXTENSION;

		// Check the file size
		if($file['size'] > $maxSize)
			return self::ERROR_UPLOAD_TOO_BIG;

		// Upload the file
		$path 										= explode('/', $uploadedName);
		$fileTitle 									= array_pop($path);
		$path 										= UPLOAD_ROOT.implode('/', $path);

		if(!is_dir($path))
		{
			if(!mkdir($path, 0777, true))
				return self::ERROR_UPLOAD_MOVE;
		}

		// Remove a previous file, if any
		if(!empty($this->getUploaded_filename()))
			unlink(UPLOAD_ROOT.$this->getUploaded_filename());

		move_uploaded_file($file['tmp_name'], $path."/{$fileTitle}.{$extension}");

		$this->setUploaded_filename($uploadedName.'.'.$extension);
		$this->setDate_uploaded(strtotime('now'));

		return true;
	}
}