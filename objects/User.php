<?php
namespace Frost\Object;

/**
* The Frost user object
*/
class User extends \Frost\Db\dbObject
{
	protected function getTableName()
	{
		return 'frost_user';
	}

	/**
	 * Gets an Frost user object by email address
	 * @param string $email The email to search for
	 * @return User The Frost user object, NULL on fail
	 */
	public static function getByEmail($email)
	{
		$db 										= new \Frost\Db\db();

		$id 										= $db->prepareExecute('SELECT id FROM frost_user WHERE email = :email', [ 'email' => $email ])->fetch(\PDO::FETCH_COLUMN);

		if(!$id)
			return NULL;

		return new User($id);
	}

	/**
	 * Gets an array of Frost user IDs where disabled
	 * @param bool $invert Inverts the search, getting active accounts
	 * @return mixed Array on success, otherwise NULL
	 */
	public static function getByDisabled($invert = false)
	{
		$db 										= new \Frost\Db\db();

		$ids 										= $db->prepareExecute('SELECT id FROM frost_USER WHERE disabled = :state', [ 'state' => ($invert) ? 0 : 1 ])->fetchAll(\PDO::FETCH_COLUMN);

		if(!$ids)
			return NULL;

		return \Frost\dbObject::arrayToObjects('User', $ids);
	}

	/**
	 * Gets a array of all users
	 */
	public static function getAll()
	{
		$db 										= new \Frost\Db\db();

		return $db->prepareExecute('SELECT id, email, name, admin, disabled FROM frost_user')->fetchAll(\PDO::FETCH_ASSOC);

	}

	/**
	 * Encrypts & sets the user password
	 * @param string $password The password to set
	 */
	public function setPassword($password)
	{
		parent::setPassword(password_hash($password, PASSWORD_BCRYPT));
	}

	/**
	 * Verifys if the password matches the user password
	 * @param string $password The password to check
	 * @return bool True on match
	 */
	public function verifyPassword($password)
	{
		return password_verify($password, $this->getPassword());	
	}
}