<?php
if(isset($_GET['install']))
	doInstall();
else
	printPage();

function printPage($complete = false)
{
	$css = <<< CSS
		@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400');
	
		* {
			font-weight 				: 300;
		}
	
		body {
			font-family 				: 'Open Sans', sans-serif;
			font-size 					: 12px;
			background 					: #ebebeb;
		}
		
		button {
			padding 					: 10px 20px 10px 20px;
			background 					: #37a9f3;
			color 						: #FFF;
			border 						: none;
		}

		button:hover {
			background 					: #4fb3f4;
			cursor 						: pointer;
		}
		
		section {
			position                    : absolute;
			top                         : 50%;
			left                        : 50%;
			width                       : 300px;
			height                      : 400px;
			padding 					: 5px 20px;
			transform                   : translate(-50%, -50%);
			background 					: #FFF;
			text-align 					: center;
			box-sizing 					: border-box;
			box-shadow 					: 0px 0px 10px 0px #d0d0d0;
		}
		
		section h1 {
			font-size 					: 2em;
			border-bottom 				: 1px solid #000;
		}

		section button {
			
		}
		
		table {
			margin 						: 0 auto;
			font-size 					: 1em;
		}
		
		table tr td {
			padding 					: 5px;
		}
		
		table tr td:first-child {
			font-weight 				: bold;
			text-align 					: right;
		}		
CSS;

	if(!$complete)
	{
		$dbHost = \Frost\Config::getSetting('DB_HOST');
		$dbName = \Frost\Config::getSetting('DB_NAME');
		$dbUsername = \Frost\Config::getSetting('DB_USERNAME');

		$html = <<< HTML
			<p>
				The following will install Frost to the database:.
				<table>
					<tr>
						<td>Host</td>
						<td>{$dbHost}</td>
					</tr>
					<tr>
						<td>Name</td>
						<td>{$dbName}</td>
					</tr>
					<tr>
						<td>User</td>
						<td>{$dbUsername}</td>
					</tr>
				</table>
			</p>
			<a href="?install"><button id="install">Install</button></a>	
HTML;
	}
	else
	{
		$html = <<< HTML
			<h2>Install complete!</h2>
			<p>
				You are already to use frost!
			</p>
HTML;
	}

	echo <<< HTML
		<html>
			<head>
				<title>Frost Installer</title>
				<style type="text/css">{$css}</style>
			</head>
			<body>
				<section>
					<h1>Frost Installer</h1>
					{$html}
				</section>
			</body>
		</html>
HTML;
}

function doInstall()
{
	$sql = file_get_contents('../frost/installer/database.sql');

	$db = new \Frost\Db\db();
	$db->prepareExecute($sql);

	printPage(true);
}