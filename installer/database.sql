CREATE TABLE frost_user
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    email VARCHAR(128) NOT NULL,
    password VARCHAR(255) NOT NULL,
    name VARCHAR(128) NOT NULL,
    admin TINYINT(1) NOT NULL,
    verify_code VARCHAR(128) NOT NULL,
    disabled TINYINT(1) NOT NULL
);

CREATE TABLE frost_page
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(128) NOT NULL,
    url VARCHAR(128) NOT NULL,
    template VARCHAR(128) NOT NULL,
    content TEXT,
    keywords VARCHAR(256),
    description VARCHAR(256),
    creator BIGINT(20) unsigned NOT NULL,
    modifier BIGINT(20) unsigned NOT NULL,
    created_date DATETIME,
    modified_date DATETIME,
    active TINYINT(1) NOT NULL,
    CONSTRAINT frost_page_ibfk_1 FOREIGN KEY (creator) REFERENCES frost_user (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT frost_page_ibfk_2 FOREIGN KEY (modifier) REFERENCES frost_user (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX creator ON frost_page (creator);
CREATE INDEX modifier ON frost_page (modifier);

CREATE TABLE frost_file_category
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    parent BIGINT(20) unsigned,
    name VARCHAR(128) NOT NULL,
    CONSTRAINT frost_file_category_parent_FK FOREIGN KEY (parent) REFERENCES frost_file_category (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX parent ON frost_file_category (parent);

CREATE TABLE frost_file
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category BIGINT(20) unsigned NOT NULL,
    title VARCHAR(255) NOT NULL,
    uploaded_filename VARCHAR(255) NOT NULL,
    date_uploaded INT(11) NOT NULL,
    CONSTRAINT frost_file_ibfk_1 FOREIGN KEY (category) REFERENCES frost_file_category (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX category ON frost_file (category);

CREATE TABLE frost_widget
(
    id BIGINT(20) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    value TEXT,
    properties TEXT,
    type VARCHAR(128) NOT NULL,
    page BIGINT(20) unsigned,
    container VARCHAR(128) NOT NULL,
    position INT(11) NOT NULL,
    CONSTRAINT frost_widget_ibfk_1 FOREIGN KEY (page) REFERENCES frost_page (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX page ON frost_widget (page);