<?php
namespace Frost\Db;

abstract class dbObject
{
 	protected $db;
 	protected $idColumn;

	private $tableBuffer; // A buffer used to store the table row
	private $isAutoIncrement;

	protected abstract function getTableName();

	/**
	 * Initalises a dbObject. Loads the row columns into a buffer for modification
	 * @param mixed $objId The row primay key
	 * @param string $idColumn The id column name
	 */
	public function __construct($objId = NULL, $idColumn = 'id')
	{
		// Load the table data
		$this->db 									= new db();

		// Remember the ID column
		$this->idColumn 							= $idColumn;

		$tableRow 									= $this->db->prepareExecute('SELECT * FROM '.$this->getTableName().' WHERE '.$idColumn.' = :id', [ 'id' => $objId ])->fetch(\PDO::FETCH_ASSOC);

		if(empty($tableRow))
		{
			$columns 								= $this->db->prepareExecute('DESCRIBE '.$this->getTableName())->fetchAll(\PDO::FETCH_ASSOC);
			foreach($columns as $column)
				$this->tableBuffer[$column['Field']]= '';
		}
		else
			$this->tableBuffer 						= $tableRow;

		$this->tableBuffer 							= array_change_key_case($this->tableBuffer, CASE_LOWER);

		$this->isAutoIncrement 						= false;
	}

	/**
	 * Converts an array of object IDs into dbObjects
	 * @param string $object The object to convert to
	 * @param array $ids The IDs to convert
	 * @return array An array of dbObjects
	 */
	public static function arrayToObjects($object, array $ids)
	{
		return array_map(function($id) use($object) {
			return new $object($id);
		}, $ids);
	}

	/**
	 * The get/set function
	 */
	public function __call($method, $args)
	{
		$prefix 									= strtolower(substr($method, 0, 3));
		$function 									= strtolower(substr($method, 3));

		switch($prefix)
		{
			case 'get':
				return $this->getColumn($function);
				break;
			case 'set':
				$this->setColumn($function, $args[0]);
				break;
		}
	}

	/**
	 * The ID column is auto assigned an ID by the database
	 */
	public function idIsAutoIncrement()
	{
		$this->isAutoIncrement 						= true;
	}

	/**
	 * Saves a dbObject to the database
	 */
	public function save()
	{
		// If the id column is empty, remove it
		if(empty($this->tableBuffer[strtolower($this->idColumn)]))
			unset($this->tableBuffer[strtolower($this->idColumn)]);

		$columns 									= implode(', ', array_keys($this->tableBuffer));
		$valuesLabels								= implode(', :', array_keys($this->tableBuffer));

		$values 									= [ ];
		$updateSql 									= [ ];

		foreach($this->tableBuffer as $key => $value)
		{
			if(empty($value) && !is_integer($value) && !is_string($value))
				$value 								= NULL;

			$values[$key] 							= $value;
			$updateSql[]							= "{$key} = :{$key}";
		}

		$this->db->prepareExecute('INSERT INTO '.$this->getTableName().' ('.$columns.') VALUES (:'.$valuesLabels.') ON DUPLICATE KEY UPDATE '.implode(', ', $updateSql), $values);
		$this->tableBuffer[$this->idColumn] 					= $this->db->getLastInsertId();
	}

	/**
	 * Deletes a dbObject from the database
	 */
	public function delete()
	{
		$result 									= $this->db->prepareExecute("DELETE FROM {$this->getTableName()} WHERE id = :id", [ 'id' => $this->tableBuffer['id'] ]);

		if($result === db::DB_ERROR)
			return false;

		return true;
	}

	/**
	 * Returns the table buffer array
	 * @return array THe table buffer
	 */
	public function getAsArray()
	{
		return $this->tableBuffer;
	}

	/**
	 * Gets a column value
	 * @return mixed The column value
	 */
	private function getColumn($columnName)
	{
		$columnName 								= strtolower($columnName);

		if(!array_key_exists($columnName, $this->tableBuffer))
			throw new FrostException("Cannot find dbObject column '{$columnName}'");

		return $this->tableBuffer[$columnName];
	}

	/**
	 * Sets a column value
	 * @param string $column The column to set
	 * @param mixed $value The value to set
	 */
	private function setColumn($column, $value)
	{
		$column 									= strtolower($column);

		if(!array_key_exists($column, $this->tableBuffer))
			throw new FrostException("Cannot find dbObject column '{$column}'");

		$this->tableBuffer[$column] 				= $value;
	}
}