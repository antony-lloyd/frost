<?php
namespace Frost;

class Router
{
	const ROUTE_GET 						= 0;
	const ROUTE_POST 						= 1;

	private $routes; // [Route] => [ Method, data, controller ]

	private static $currentRouteInfo 		= [ ];

	public function __construct()
	{
		$this->routes 						= [ ];
	}

	/**
	 * Creates a GET request
	 * @param $route The URL route e.g. /user/:user_id
	 * @param Hmvc\Controller $controller The controller class
	 * @throws FrostException
	 */
	public function get($route, Hmvc\Controller $controller)
	{
		// Check if the controller is valid
		if(!is_a($controller, '\Frost\Hmvc\Controller'))
			throw new FrostException('The route controller is not a valid Frost Base');

		$parts								= $this->getRouteParts($route);

		$this->routes[$parts['route']]		= [
			'method' 		=> self::ROUTE_GET,
			'data' 			=> $parts['arguments'],
			'controller' 	=> $controller
		];
	}
	
	/**
	 * Creates a POST request
	 * @param string $route The URL route
	 * @param array $expectedPost The expected post keys
	 * @param Hmvc\Controller $controller The controller class
	 * @throws FrostException
	 */
	public function post($route, array $expectedPost, Hmvc\Controller $controller)
	{
		// Check if the controller is valid
		if(!is_a($controller, '\Frost\Hmvc\Controller'))
			throw new FrostException('The route controller is not a valid Frost Base');

		$parts								= $this->getRouteParts($route);
		
		$this->routes[$parts['route']]		= [
			'method' 		=> self::ROUTE_POST,
			'data' 			=> $expectedPost,
			'controller' 	=> $controller
		];
	}
	
	/**
	 * Routes the URL to a controller
	 * @param type $url The URL to route
	 */
	public function route()
	{	
		$url  								= $_SERVER['REQUEST_URI'];

		// Check if there is a trailing '/'
		if($url[strlen($url) - 1] != '/')
			$url .= '/';
		
		// Check if loading the root
		if($url == '/')
			$url 							= '//';

		// Sort the route array by route length descending, so sub-routes can be found
		$keys								= array_map('strlen', array_keys($this->routes));
		array_multisort($keys, SORT_DESC, $this->routes);

		foreach($this->routes as $route => $data)
		{
			if(substr($url, 0, strlen($route)) == $route)
			{
				$controller 				= $data['controller'];

				// Check what method to use
				if($data['method'] == self::ROUTE_GET)
				{
					$args					= $this->getDataFromUrl($url, $data['data']);

					$controller->setData($args);
				}
				else if($data['method'] == self::ROUTE_POST)
				{
					$postData				= $this->getValuesFromPost($data['data']);
					
					$controller->setData($postData);
				}

				// Set the current route info
				self::$currentRouteInfo 	= array_values(array_filter(explode('/', $route)));

				$controller->setRoute($route);
				$controller->run();
				return;
			}
		}

		// If no route is loaded, load the wildcard (/*)
		if(in_array('/*/', array_keys($this->routes)))
		{
			self::$currentRouteInfo 	= array_values(array_filter(explode('/', $route)));

			$controller 					= $this->routes['/*/']['controller'];
			$controller->setRoute('*'.$url);
			$controller->run();
		}
	}
	
	/**
	 * Gets the route parts from a string. Each part is seperated with a '/', arguments begin with ':'.
	 * @param type $route The route to get parts from
	 * @return type An array containing the resource & the arguments
	 */
	private function getRouteParts($route)
	{
		$parts							= explode('/', $route);

		// All variables are at end of route
		$firstVar 						= strpos($route, ':');
		$routePath 						= substr($route, 0, $firstVar == false ? strlen($route) : $firstVar);

		if(empty($routePath))
			$routePath					= '/';
		
		// Get all arguments from the route
		$args						= array_map(function($part) {
			// If its not a argument, leave it blank
			if(empty($part))
				return;
			
			if($part[0] == ':')
				return substr($part, 1);
		}, $parts);
		
		// Remove empty values
		$args = array_filter($args);
		
		// Change the index to the name of the argument
		foreach($args as $index => $value)
		{
			$args[substr($parts[$index], 1)]	= [ 'index' => $index, 'value' => $value ];
			unset($args[$index]);
		}
		
		return [
			'route'						=> $routePath,
			'arguments'					=> $args
		];
	}
	
	/**
	 * Gets the route data from a URL
	 * @param string $url The URL to use
	 * @param array $expectedArgs An array of expected data
	 * @return type An array of arguments, with the argument name as the key
	 */
	private function getDataFromUrl($url, array $expectedArgs)
	{
		$args 							= [ ];
		
		$parts							= explode('/', $url);
		
		foreach($expectedArgs as $key => $value)
		{
			if(isset($parts[$value['index']]))
				$args[$key]				= $parts[$value['index']];
		}

		return filter_var_array($args, FILTER_SANITIZE_STRING);
	}
	
	/**
	 * Gets the allowed values from POST
	 * @param array $allowed An array of allowed POST keys
	 * @return array An array of POST items
	 */
	private function getValuesFromPost(array $allowed)
	{
		$_POST 							= filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		$args							= [ ];

		if(empty($_POST) || empty($allowed))
			return $args;
		
		foreach($allowed as $key => $value)
		{
			if(!isset($_POST[$value]))
				$args[$value] 			= '';
			else
				$args[$value]			= $_POST[$value];
		}

		$_POST 							= [ ];
		
		return $args;
	}

	/**
	 * Gets the current route info
	 * @return array
	 */
	public static function getCurrentRouteInfo()
	{
		return self::$currentRouteInfo;
	}
}