<?php
namespace Frost\Auth;

class Security
{
	/**
	 * Generates a random string
	 * @param int $length The length of the string. If less than 1, defaults to 10
	 * @return string A random string
	 */
	public static function generateRandomString($length = 10)
	{
		$chars 										= array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));

		if($length < 1)
			$length 								= 10;

		$string 									= '';

		for($i = 0; $i < $length; $i++)
			$string 								.= $chars[rand(0, count($chars) - 1)];

		return $string;
	}
}