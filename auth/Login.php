<?php
namespace Frost\Auth;

class Login
{
	const ERROR_ALL_FIELDS_REQUIRED 				= 0;
	const ERROR_INCORRECT_FIELDS 					= -1;
	const ERROR_VERIFY_EMAIL 						= -2;
	const ERROR_ADMIN_REQUIRED 					 	= -3;
	const ERROR_DISABLED							= -4;

	private static $db;

	/**
	 * Logs in a Frost user
	 * @param string $email The email of the user
	 * @param string $password The password of the user
	 * @param bool $adminRequired Check if the Frost user is a admin
	 * @return int True on success, otherwise a Frost login error code
	 */
	public static function login($email, $password, $adminRequired = false)
	{
		self::$db			 						= new \Frost\Db\db();

		if(!$email || !$password)
			return self::ERROR_ALL_FIELDS_REQUIRED;

		$email 										= filter_var($email, FILTER_SANITIZE_STRING);
		$password 									= filter_var($password, FILTER_SANITIZE_STRING);

		$user 										= \Frost\Object\User::getByEmail($email);

		if($user === NULL)
			return self::ERROR_INCORRECT_FIELDS;

		if(!$user->verifyPassword($password))
			return self::ERROR_INCORRECT_FIELDS;

		if(\Frost\Config::getSetting('REGISTER_VERIFY') == true && strlen($user->getVerify_code()) != 0)
			return self::ERROR_VERIFY_EMAIL;

		if($user->getAdmin() == 0 && $adminRequired)
			return self::ERROR_ADMIN_REQUIRED;

		if($user->getDisabled() == 1)
			return self::ERROR_DISABLED;

		$_SESSION['FROST_USER'] 					= [
			'id'		=> $user->getId(),
			'email'		=> $user->getEmail(),
			'name'		=> $user->getName(),
			'admin' 	=> $user->getAdmin() == 1 ? true : false
		];

		return true;
	}

	/**
	 * Logs out a frost user
	 * @param function $callback The function to call on logout
	 */
	public static function logout($callback = NULL)
	{
		session_unset();
		session_destroy();

		if(isset($callback))
			$callback();
	}

	/**
	 * Checks if a user is logged in
	 * @return bool
	 */
	public static function isLoggedIn()
	{
		return isset($_SESSION['FROST_USER']);
	}

	/**
	 * Checks if a user is logged in, if not redirects to the login page
	 */
	public static function requiresAuth()
	{
		if(!self::isLoggedIn())
		{
			header('location: '.\Frost\Config::getSetting('URI_LOGIN'));
			die();
		}
	}

	/**
	 * Gets the logged in user id
	 * @return null
	 */
	public static function getUserId()
	{
		if(!self::isLoggedIn())
			return NULL;

		return $_SESSION['FROST_USER']['id'];
	}

	/**
	 * Gets the logged in user email
	 * @return null
	 */
	public static function getUserEmail()
	{
		if(!self::isLoggedIn())
			return NULL;

		return $_SESSION['FROST_USER']['email'];
	}

	/**
	 * Gets the logged in user name
	 * @return null
	 */
	public static function getUserName()
	{
		if(!self::isLoggedIn())
			return NULL;

		return $_SESSION['FROST_USER']['name'];
	}

	/**
	 * Gets the logged in user admin
	 * @return null
	 */
	public static function getUserAdmin()
	{
		if(!self::isLoggedIn())
			return NULL;

		return $_SESSION['FROST_USER']['admin'];
	}
}