<?php
namespace Frost\Auth;

class Register
{
	const ERROR_ALL_FIELDS_REQUIRED					= 0;
	const ERROR_EMAIL_USED 							= -1;
	const ERROR_CREATING 							= -2;
	const ERROR_EMAIL_NOT_EXIST 					= -3;
	const ERROR_VERIFY_INVALID_CODE 				= -4;

	private static $db;

	/**
	 * Registers a new Frost user
	 * @param string $name The name of the user
	 * @param string $email The email of the user
	 * @param string $password The password of the user
	 * @return int True on success, otherwise a Frost Register Error code
	 */
	public static function register($name, $email, $password)
	{
		self::$db			 						= new \Frost\Db\db();

		if(!$name || !$email || !$password)
			return self::ERROR_ALL_FIELDS_REQUIRED;
		else
		{
			$name 									= filter_var($name, FILTER_SANITIZE_STRING);	
			$email 									= filter_var($email, FILTER_SANITIZE_STRING);
			$password 								= filter_var($password, FILTER_SANITIZE_SPECIAL_CHARS);

			if(\Frost\Object\User::getByEmail($email) != NULL)
				return self::ERROR_EMAIL_USED;
			else
			{
				if(\Frost\Config::getSetting('REGISTER_VERIFY') == "true")
					$verifyCode 					= \Frost\Auth\Security::generateRandomString();
				else
					$verifyCode 					= '';

				$user 								= new \Frost\Object\User();

				$user->setEmail($email);
				$user->setPassword($password);
				$user->setName($name);
				$user->setAdmin(0);
				$user->setVerify_code($verifyCode);
				$user->setDisabled(0);
				$user->save();

				if(!empty($verifyCode))
				{
					$mail 							= new \Frost\Email();
					$mail->sendSimple($email, \Frost\Config::getSetting('SITE_NAME').' Account Verification', <<< TEXT
						Thank you for registering, please verify your account. Your code: {$verifyCode}
TEXT
					, \Frost\Config::getSetting('EMAIL_FROM'));
				}

				return true;
			}
		}
	}

	/**
	 * Verifys a Frost user account
	 * @param string $email The user email
	 * @param string $code The verify code
	 * @return bool
	 */
	public static function verify($email, $code)
	{
		self::$db			 						= new \Frost\Db\db();

		if(!self::$db->isInDatabase('frost_user', 'email', $email))
			return self::ERROR_EMAIL_NOT_EXIST;

		$user 										= self::$db->prepareExecute('SELECT * FROM frost_user WHERE email = :email', [ 'email' => $email ])->fetch(\PDO::FETCH_ASSOC);

		if($user['verify_code'] != $code)
			return self::ERROR_VERIFY_INVALID_CODE;
		else
		{
			self::$db->prepareExecute('UPDATE frost_user SET verify_code = \'\' WHERE email = :email', [ 'email' => $email ]);
			return true;
		}
	}
}