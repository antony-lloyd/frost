<?php
namespace Frost\Hmvc;

/**
 * The Frost controller abstract class. Used to create app controllers
 */
abstract class Controller
{
	private $data;
	private $route;

	protected $view;
	protected $model;

	public function __construct()
	{
		$this->data 						= [ ];

		$this->route 						= '';

		$this->view 						= NULL;
		$this->model 						= NULL;
	}

	/**
	 * Sets the controller data
	 * @param string $data The array of data
	 */
	public function setData(array $data)
	{
		$this->data 						= $data;
	}

	/**
	 * Sets the route used for the controller
	 * @param string $route The route to set
	 */
	public function setRoute($route)
	{
		$this->route 						= $route;
	}

	/**
	 * Checks if the request is AJAX
	 */
	public function isRequestAjax()
	{
		$headers 							= apache_request_headers();

		return (!empty($headers['HTTP_X_REQUESTED_WITH']) && strtolower($headers['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}

	/**
	 * Gets the controller data
	 * @return array An array of data
	 */
	protected function getData()
	{
		return $this->data;
	}

	/**
	 * Gets the route used by the controller
	 * @return string The controller route
	 */
	protected function getRoute()
	{
		return $this->route;
	}

	/**
	 * Gets the route parts used by the controller
	 * @return array The controller route parts
	 */
	protected function getRouteParts()
	{
		return array_values(array_filter(explode('/', $this->getRoute())));
	}

	/**
	 * The main entry point of the controller
	 */
	abstract public function run();
}