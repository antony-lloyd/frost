<?php
namespace Frost\Hmvc;

/**
 * The page class creates a new standard web page
 */
class View
{
	const MODE_EDIT 						= 0;
	const MODE_RENDER 						= 1;

	////////////////////////
	// Private variables
	////////////////////////
	private $externalCssSheets; // <link>...
	private $externalJavaScript; // <script src="">...
	private $internalCss; // <style>...
	private $internalJavascript; // <script>...

	private $title;
	private $content;

	private $pageMode;

	/**
	 * Create a new web page
	 */
	public function __construct()
	{
		$this->externalCssSheets 			= [ ];
		$this->externalJavaScript 			= [ ];
		$this->internalCss 					= [ ];
		$this->internalJavascript 			= [ ];

		$this->content 						= '';

		$this->pageMode 					= self::MODE_RENDER;
	}

	/**
	 * Add a CSS file to the page
	 * @param string $css The href to the stylesheet
	 * @param bool $external The URL is external to the system
	 */
	public function addCss($css, $external = false)
	{
		if(!$external)
			$url 							= '/public/'.$css;
		else
			$url 							= $css;

		$this->externalCssSheets[] 			= "<link type='text/css' rel='stylesheet' href='{$url}' />";
	}

	/**
	 * Adds a theme CSS file
	 * @param $css The file name
	 */
	public function addThemeCss($css)
	{
		$url 								= THEME_RELATIVE_ROOT.\Frost\Config::getSetting('SITE_THEME')[$GLOBALS['FROST']['APP_NAME']].'/css/'.$css;
		$this->externalCssSheets[] 			= "<link type='text/css' rel='stylesheet' href='{$url}' />";
	}

	/**
	 * Add a JS file to the page
	 * @param string $js The href to the script file
	 * @param bool $external The URL is external to the system
	 */
	public function addJavaScript($js, $external = false)
	{
		if(!$external)
			$url 							= '/public/'.$js;
		else
			$url							= $js;

		$this->externalJavaScript[] 		= "<script type='text/javascript' src='{$url}'></script>";
	}

	/**
	 * Adds a theme JavaScript file
	 * @param $js The file name
	 */
	public function addThemeJavaScript($js)
	{
		$url 								= THEME_RELATIVE_ROOT.\Frost\Config::getSetting('SITE_THEME')[$GLOBALS['FROST']['APP_NAME']].'/js/'.$js;
		$this->externalJavaScript[] 		= "<script type='text/javascript' src='{$url}'></script>";
	}

	/**
	 * Adds instyle CSS to the page
	 * @param $css The css to add
	 */
	public function addInternalCss($css)
	{
		$this->internalCss[] 				= '<style type="text/css">'.$css.'</style>';
	}

	/**
	 * Adds instyle JavaScript to the page
	 * @param $js The JavaScript to add
	 */
	public function addInternalJavascript($js)
	{
		$this->internalJavascript[] 		= '<script type="text/javascript">'.$js.'</script>';
	}

	/**
	 * Set the title of the web page
	 * @param string $title The title of the web page
	 */
	public function setTitle($title)
	{
		$this->title 						= $title;
	}

	/**
	 * Set the content of the web page
	 * @param string $html The HTML of the content
	 */
	public function setContent($html)
	{
		$this->content 						= $html;
	}

	/**
	 * Returns the title of the web page
	 * @return string The title of the web page
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Returns the entire HTML of the page
	 * @return string The page HTML
	 */
	public function getPage()
	{
		return $this->getHead().$this->getContentHtml();
	}

	/**
	 * Prints the HTML page and terminates execution
	 * @param bool $continueExeuction Default false. True if to continue execution
	 */
	public function printPage($continueExeuction = false)
	{
		echo $this->getPage();
		if(!$continueExeuction) die();
	}

	/**
	 * Sets the page mode
	 * @param $mode int The mode to set
	 */
	public function setMode($mode)
	{
		$this->pageMode 					= $mode;
	}

	/**
	 * Gets the page mode
	 * @return int The page mode
	 */
	public function getMode()
	{
		return $this->pageMode;
	}

	/**
	 * Gets the page content
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * Gets the head HTML of the page
	 * @return string The head HTML
	 */
	private function getHead()
	{
		$head 								= <<< HTML
			<html>
				<head>
					<title>{$this->title}</title>
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
HTML;
		$head 								.= implode('', $this->externalCssSheets).implode('', $this->internalCss);
		$head 								.= implode('', $this->externalJavaScript).implode('', $this->internalJavascript);

		$head 								.= "</head>";

		return $head;
	}

	/**
	 * Gets the page content
	 * @return string The content HTML
	 */
	private function getContentHtml()
	{
		return <<< HTML
			</head>
			<body>
				{$this->content}
			</body>
		</html>
					
HTML;
	}
};